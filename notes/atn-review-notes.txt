This extension is used to manipulate the content of email before sending.
The HTML editor must be enabled.

REPRODUCING THE BUILD

Requirements:
nodejs: 20.x
pnpm: 9.x
GNU Make
tar
A UNIX style shell (bash)

- Untar the uploaded source
- Run "make all"

Running "make all" in the unpacked source directory will rebuild
mailext-options-sync and copy the final output into extension/options/. That
output should match the version from the mailext-options-sync repository
on  Gitlab (URL to the exact tag is below).
Then web-ext build will generate the extension XPI file and place it in
web-ext-artifacts.

I noticed that the md5sum of the XPI I uploaded does not match what gets
generated. The contents of the files within report no differences when diff'd.

THIRD PARTY:

Highlightjs is used for syntax highlighting source code in the rendered
HTML.
It's not the easiest thing to download and customize what languages are
supported, so I wrote tools/get-highlightjs.sh to build from source.
That script also generates a styles.json file which is used in the
options page in the syntax style selection.

marked.esm.js
https://raw.githubusercontent.com/markedjs/marked/v5.1.2/lib/marked.esm.js

marked-extended-tables.esm.js:
https://raw.githubusercontent.com/calculuschild/marked-extended-tables/main/src/index.js

marked-highlight.esm.js:
https://raw.githubusercontent.com/markedjs/marked-highlight/v2.0.1/src/index.js

marked-linkify-it.esm.js:
This and "linkify-it" are installed in node_modules via pnpm and then mangled
into an esm module with "esbuild". `make marked-linkify-it` will handle all of
this.

marked-smartypants.esm.js:
This and "smartypants" are installed in node_modules via pnpm and then mangled
into an esm module with "esbuild". `make marked-smartypants` will handle all
of this.

degausser.esm.js:
Installed in node_modules via pnpm and then built with esbuild. `make degausser`
handles this.

turndown.esm.js:
Installed in node_modules via pnpm. The vendor esm build is copied via `make turndown`.

Emoji stuff:
Installed in node_modules via pnpm. The vendor esm build is copied via `make turndown`.

marked-emoji.esm.js:
https://raw.githubusercontent.com/UziTech/marked-emoji/v1.2.2/src/index.js

TeXZilla.js
https://raw.githubusercontent.com/fred-wang/TeXZilla/v1.0.2.0/TeXZilla.js
(modified to be importable as an esm module)

jsHtmlToText.js
I cannot locate a current version of this library, so this is the same
one from the markdown-here repository. There are several candidates to
replace it.

Use of mailextension experiments:
The notificationbar experiment from https://github.com/jobisoft/notificationbar-API
is used. Git rev 8f645332bca46c81b6f0108fcfe7f7bf1bcec2f4 plus
https://github.com/jobisoft/notificationbar-API/pull/20.

Options UI pages:
- mailext-options-sync:
  This is a project that I forked and removed some functionality from. The final built
  file that's included in the extension can be downloaded from:
  https://gitlab.com/jfx2006/mailext-options-sync/-/raw/v2.1.4/mailext-options-sync.js

  Additionally, the source for the included version is in the uploaded tar file.
  The MDHR Makefile will build mailext-options-sync and copy it into extension/options.
  Version 2.1.2 was built with node 14.17.0 and npm 6.14.13

https://raw.githubusercontent.com/twbs/bootstrap/v5.2.2/dist/js/bootstrap.bundle.js
https://raw.githubusercontent.com/thomaspark/bootswatch/v5.2.2/dist/darkly/bootstrap.css
Parts of shortcuts.js from Firefox source code:
    https://hg.mozilla.org/mozilla-central/file/9d0d26dacd7f8d76a7805cffb98faec5cd6aa7fa/toolkit/mozapps/extensions/content/shortcuts.js

DOMPurify 2.4.3 (ESM build)
https://raw.githubusercontent.com/cure53/DOMPurify/2.4.3/dist/purify.es.js

Used for tests
https://cdn.jsdelivr.net/npm/mocha@8.1.3/mocha.js
https://cdn.jsdelivr.net/npm/mocha@8.1.3/mocha.css
https://raw.githubusercontent.com/chaijs/chai/4.3.1/chai.js
https://raw.githubusercontent.com/nathanboktae/chai-dom/v1.11.0/chai-dom.js
https://sinonjs.org/releases/sinon-16.1.0.js
https://code.jquery.com/jquery-3.6.0.slim.js



USING/TESTING:

You write email in Markdown, then click the button in the format toolbar
and the content of the email will be replaced with a rendered-HTML version
of the Markdown you had written.

If you select a subset of your email and then "Markdown Toggle", only
that selection will be rendered/converted.

The Markdown is "Github-flavored". Syntax highlighting is supported. So,
for example, you can use fenced code blocks with the language name
specified, like so:

```javascript
var i = 0;
```


For complete info, see the project page:
https://gitlab.com/jfx2006/markdown-here-revival

This project is a fork of Markdown Here which has not worked on Thunderbird
since Thunderbird 60. Support for browser-based email and other mail clients
is being dropped to focus on giving Thunderbird first class Markdown
support going forward.

Please don't hesitate to contact
me with questions.


A more formal test methodology, if desired:

1. Make sure HTML composing is enabled.

2. Write a bit of Markdown. (Example below)

3. Click the toggle button.

Test #1:
The entire email should be converted to HTML, in accordance with Markdown
syntax. The sample MD should have H1, H3, UL, and PRE/CODE elements, with
some bold and code inline.

4. Click "Markdown Toggle" again.

Test #2:
The email should be reverted back the original raw Markdown.

5. Select a few lines (but don't split the code block). Then click
"Markdown Toggle".

Test #3:
Only the selected lines should be rendered to HTML.

6. Click "Markdown Toggle" again.

Test #4:
The  email should be reverted back to original Markdown.

7. Render the email again (either entirely or partially) and send it
(to yourself or someone else).

Test #5:
The recipient of the email should see it pretty much as it was before you
sent it.

Test done.



A few secondary features that can be tested if desired:

- The rendered email

- Options: Should auto-save.
- Notification bar on upgrade that can open an internal documentation page
  (which is not written yet)
- Signatures should be excluded from the conversion.
Sigs must be preceded by the standard-ish '-- ' (note the trailing space).

- Images embedded in the email before conversion (Gmail and Thunderbird)
should be retained by the conversion.
- Quoted reply text should be excluded by the conversion.
- TeX mathematical formulae should be rendered if and only if that
functionality is enabled in the options.


---

# Markdown Sample (H1)

### Smaller header (H3)

* Bullet **with bold**
    * Inner list
    * Inner list 2
    * Inner list 3
        * Another inner list
        * Yet another one
    * Inner list 4
* Bullet with `inline code`

```javascript
var s = 'code block';

console.log(s);
```

This is the quadratic equation in TeX (rendered if
enabled):
`$-b \pm \sqrt{b^2 - 4ac} \over 2a$`

(Note this feature is annoying as it sends an email with remote content!
I intend to fix that in a later release.)
